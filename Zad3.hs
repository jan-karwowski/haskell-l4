{-# OPTIONS -Wall #-}
module Zad3 where

import Data.List

theFilter :: [Int] -> [Int]
theFilter l = take 2 l ++  map getEl (filter pred (zip4 l (drop 1 l) (drop 2 l) [3..]))
  where
    pred (xm2, xm1, x, p)
      | p `mod` 2 == 1 = xm2+xm1 > x
      | otherwise  = x > abs (xm2-xm1)
    getEl (_, _, x, _) = x

theFilter2 :: [Int] -> [Int]
theFilter2 l = take 2 l ++  (map getEl . filter pred $ zip4 l (drop 1 l) (drop 2 l) (cycle [False, True]))
  where
    pred (xm2, xm1, x, p)
      | not p = xm2+xm1 > x
      | otherwise  = x > abs (xm2-xm1)
    getEl (_, _, x, _) = x


theFilter'':: [Int] -> [Int]
theFilter'' = do
  f <- take 2
  r <- zipWith4 calculateCond (cycle [True,False]) <$> drop 2 <*> drop 1 <*> id
  return $ f ++ (map fst . filter snd) r
  where
    calculateCond True el prev2 prev1 = (el, el<prev1+prev2)
    calculateCond False el prev2 prev1 = (el, el>abs (prev1-prev2))
