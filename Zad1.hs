{-# OPTIONS -Wall #-}
module Zad1 where

data PowSet = Contains | NotContains | Element Integer PowSet PowSet deriving Show

addElement :: [Integer] -> PowSet -> PowSet
addElement [] NotContains = Contains
addElement [] Contains = Contains
addElement [] (Element val left right) = Element val left $ addElement [] right
addElement (x:xs) NotContains = Element x (addElement xs NotContains) NotContains
addElement (x:xs) Contains = Element x (addElement xs NotContains) Contains
addElement set@(x:xs) subtree@(Element val left right)
 | x==val = Element val (addElement xs left) right
 | x < val = Element x (addElement xs NotContains) subtree
 | otherwise = Element val left (addElement set right)

containsElement :: [Integer] -> PowSet -> Bool
containsElement [] NotContains = False
containsElement [] Contains = True
containsElement set@(x:xs) (Element val left right)
 | val == x = containsElement xs left
 | val > x = False
 | otherwise = containsElement set right
containsElement [] (Element _ _ right) = containsElement [] right
containsElement _ _ = False


enumerateElements :: PowSet -> [[Integer]]
enumerateElements NotContains = []
enumerateElements Contains = [[]]
enumerateElements (Element val left right) = enumerateElements right ++ map (val:) (enumerateElements left)

oddSubsets :: PowSet
oddSubsets = subset 1
  where
    subset x = Element x (subset (x+2)) Contains

instance Eq PowSet where
  set1 == set2 = check set1 set2 && check set2 set1
    where
      check s1 s2 = all (`containsElement`s2) $ enumerateElements s1

sumElements :: PowSet -> Integer
sumElements set = sumR set 0 0 
  where
    sumR :: PowSet -> Integer -> Integer -> Integer
    -- subsetSum -- suma podzbioru na ścieżce
    -- totalSum -- suma wszystkiego dotąd odwiedzonego
    sumR NotContains subsetSum totalSum = totalSum
    sumR Contains subsetSum totalSum = totalSum + subsetSum
    sumR (Element val left right) subsetSum totalSum = sumR right subsetSum leftSum
      where
        leftSum = totalSum `seq` subsetSum+val `seq`  sumR left (subsetSum+val) totalSum

main=undefined
