{-# OPTIONS -Wall #-}
module Zad2 where

seqA :: [Integer]
seqB :: [Integer]

seqA = 2 : zipWith (\a b -> a-3*b) seqA seqB
seqB = 1:(-5):(-2): zipWith3 (\a1 a2 b -> -3*a1+a2-b) seqA (drop 1 seqA) (drop 1 seqB)
